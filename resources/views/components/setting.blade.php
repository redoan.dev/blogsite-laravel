@props(['heading'])
<section class="py-8 mx-auto">
    <div class="flex justify-center min-h-screen bg-gray-100">
        <div class="container sm:mt-40 mt-24 my-auto max-w-md border-2 border-gray-200 p-3 bg-white max-w-4xl">
            <div class="text-center my-6 border-b">
                <h1 class="text-3xl font-semibold text-gray-700">{{ $heading }}</h1>

            </div>
            <div class="flex">
                <x-panel>
                <aside class="w-24">
                    <h4 class="font-semibold mb-4 border-b">Links</h4>
                    <ul>
                        <li>
                            <a href="{{url('admin/posts')}}" class="{{ request()->is("admin/posts")?'text-blue-500':'' }}">All Post</a>
                        </li>
                    </ul>
                    <ul>
                        <li>
                            <a href="{{url('admin/posts/create')}}" class="{{ request()->is("admin/posts/create")?'text-blue-500':'' }}">New Post</a>
                        </li>
                    </ul>
                </aside>
                </x-panel>
                <main class="flex-1">
                    {{ $slot }}
                </main>
            </div>
        </div>
    </div>
    </div>
</section>

