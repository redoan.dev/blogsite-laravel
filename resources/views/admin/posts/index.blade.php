<x-layout>
    <x-setting heading='Manage Post'>
        <!-- This example requires Tailwind CSS v2.0+ -->
        <div class="m-6">
            <table class="mb-4 w-relative">
                <thead class="bg-gray-50">
                    <tr>
                        <th scope="col"
                            class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            Title
                        </th>
                        <th scope="col"
                            class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            Status
                        </th>
                        <th scope="col"
                            class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            Edit
                        </th>
                    </tr>
                </thead>
                <tbody class="bg-white divide-y divide-gray-200">
                    @foreach ($posts as $post)

                        <tr>
                            <td class="px-6 py-4 whitespace-nowrap">
                                <div class="text-indigo-600 hover:text-indigo-900 text-sm"><a href="/post/{{ $post->slug }}">{{ $post->title }}</a></div>
                            </td>
                            <td class="px-6 py-4 whitespace-nowrap">
                                <span
                                    class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                                    Published
                                </span>
                            </td>
                            <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                <div class="flex">
                                    <a href="/admin/posts/{{ $post->id }}/edit"
                                        class="text-indigo-600 hover:text-indigo-900 mr-2">Edit</a>
                                    <form action="/admin/posts/{{ $post->id }}" method="post">
                                        @csrf
                                        @method("DELETE")
                                        <button class="text-sx text-gray-400" onclick="return confirm('Are You Sure?')">Delete</button>
                                    </form>
                                </div>
                            </td>

                        </tr>
                    @endforeach

                    <!-- More people... -->
                </tbody>
            </table>
        </div>
    </x-setting>
</x-layout>
