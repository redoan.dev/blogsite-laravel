<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class AdminPostController extends Controller
{
    public function index(){
        return view('admin.posts.index',[
            'posts'=>Post::paginate(50)
        ]);
    }
    public function create(){
        
        return view('admin.posts.create');

    }
    public function store(Request $request){
        
        // $slug= Str::slug($request->get('title'));
        $attributes= $request->validate([
            'title'=>'required',
            'slug'=>['required', Rule::unique('posts','slug')],
            'image'=>'required|image',
            'excerpt'=>'required',
            'body'=>'required',
            'category_id'=>['required', Rule::exists('categories','id')],

        ]);
        // $attributes['slug']=$slug;
        $attributes['image']=$request->file('image')->store('image');
        $attributes['user_id']=auth()->id();
        Post::create($attributes);
        return redirect('/');
    }
    public function edit(Post $post){
        return view('admin.posts.edit',compact('post'));
    }
    public function update(Post $post){
        
        // $slug= Str::slug($request->get('title'));
        $attributes= request()->validate([
            'title'=>'required',
            'slug'=>['required', Rule::unique('posts','slug')->ignore($post->id)],
            'image'=>'image',
            'excerpt'=>'required',
            'body'=>'required',
            'category_id'=>['required', Rule::exists('categories','id')],

        ]);
        // $attributes['slug']=$slug;
        if(request()->image){
        $attributes['image']=request()->file('image')->store('image');
        }
        $post->update($attributes);
        return redirect(url('admin/posts'))->with('success','Successfully Updated!!');
    }
    public function destroy(Post $post){
        $post->delete();
        return back()->with('success','Successfully Deleted!!');
    }
}
