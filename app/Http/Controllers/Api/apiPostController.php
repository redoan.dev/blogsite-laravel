<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;

class apiPostController extends Controller
{
    public function show(Post $post)
    {
        return response()->json($post);
    }
}
